package ru.tagirov.tm.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.api.service.ProjectService;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.enumeration.Status;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Component
public class EntityCreateUtil {

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private TaskService taskService;

    public void create() throws SQLException, ParseException {

        List<User> users = new ArrayList<>();
        users.add(new User("4eb99bba-b64e-4813-a22e-8238ae93f480", "a", Md5Util.getHash("a"), Role.ADMIN));
        users.add(new User("e514f9bd-7936-4d7a-9f4c-ffaf90918eac", "name1", Md5Util.getHash("n1"), Role.USER));
        users.add(new User("71353201-df19-494f-bda9-0aa12ad9511a", "name2", Md5Util.getHash("n2"), Role.USER));
        users.add(new User("c253b64f-98fb-48c6-be5f-8cebfe00bac6", "name3", Md5Util.getHash("n3"), Role.USER));
        for (User u: users){
            userService.save(u);
        }

        List<Project> projects = new ArrayList<>();
        projects.add(new Project("bc306e1e-bf7a-495d-9544-cc367443234b", "pr_name1_u1", "pr_dis1_u1",DateUtil.getDate("11-11-2012"), DateUtil.getDate("11-12-2012"),users.get(1), Status.PLANNED));
        projects.add(new Project("deac9da4-1db8-4031-8c0c-486b0d460b7b", "pr_name2_u1", "pr_dis2_u1",DateUtil.getDate("12-11-2013"), DateUtil.getDate("12-12-2013"),users.get(1), Status.PLANNED));
        projects.add(new Project("63e12099-c519-4a0d-8b2f-60b24f82bffa", "pr_name3_u1", "pr_dis3_u1",DateUtil.getDate("13-11-2014"), DateUtil.getDate("13-12-2014"),users.get(1), Status.PLANNED));
        projects.add(new Project("bd557fa9-47d3-4a21-a638-33821340bb1c", "pr_name4_u2", "pr_dis4_u2",DateUtil.getDate("14-11-2015"), DateUtil.getDate("14-12-2015"),users.get(2), Status.PLANNED));
        projects.add(new Project("4092452e-940e-44f2-b46a-46f859888049", "pr_name5_u2", "pr_dis5_u2",DateUtil.getDate("15-11-2016"), DateUtil.getDate("15-12-2016"),users.get(2), Status.PLANNED));
        projects.add(new Project("7b040f1f-8ade-4437-b995-07955afd1ec4", "pr_name6_u2", "pr_dis6_u2",DateUtil.getDate("16-11-2017"), DateUtil.getDate("16-12-2017"),users.get(2), Status.PLANNED));
        projects.add(new Project("181a1104-6bd4-4f47-8d9e-1c4e31b3b668", "pr_name7_u3", "pr_dis7_u3",DateUtil.getDate("17-11-2019"), DateUtil.getDate("17-12-2019"),users.get(3), Status.PLANNED));
        projects.add(new Project("6ecf5a3d-8219-481d-a223-6a5dbe9ede90", "pr_name8_u3", "pr_dis8_u3",DateUtil.getDate("18-11-2019"), DateUtil.getDate("18-12-2019"),users.get(3), Status.PLANNED));
        projects.add(new Project("0b51951e-41e4-47b7-915b-6e327187f31e", "pr_name9_u3", "pr_dis9_u3",DateUtil.getDate("19-11-2020"), DateUtil.getDate("19-12-2020"),users.get(3), Status.PLANNED));
        for (Project p: projects){
            projectService.save(p);
        }

        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task("b0e031e6-5c5c-407a-990a-776fec01591f", "t_name1_u1", "t_dis1_u1",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(0),users.get(1), Status.PLANNED));
        tasks.add(new Task("de6faff6-53a3-40cf-8a56-1d4c091a7a70", "t_name2_u1", "t_dis2_u1",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(1),users.get(1), Status.PLANNED));
        tasks.add(new Task("27ff8e11-d1cf-421c-8f5b-94cbe7e70ba9", "t_name3_u1", "t_dis3_u1",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(2),users.get(1), Status.PLANNED));
        tasks.add(new Task("c9c27154-49fb-417f-80c2-2c0e1c17219b", "t_name4_u2", "t_dis4_u2",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(3),users.get(2), Status.PLANNED));
        tasks.add(new Task("af7018a2-bb1c-45cb-a271-41691f18e839", "t_name5_u2", "t_dis5_u2",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(4),users.get(2), Status.PLANNED));
        tasks.add(new Task("38c6a72c-501e-403f-a15c-54d82d9b8720", "t_name6_u2", "t_dis6_u2",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(5),users.get(2), Status.PLANNED));
        tasks.add(new Task("f5919c3e-a63f-4265-bb96-978f181622c5", "t_name7_u3", "t_dis7_u3",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(6),users.get(3), Status.PLANNED));
        tasks.add(new Task("f2077381-7206-482a-a4d0-041c3118a4aa", "t_name8_u3", "t_dis8_u3",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(7),users.get(3), Status.PLANNED));
        tasks.add(new Task("f423d876-fc73-4a30-8b6e-29334642de38", "t_name9_u3", "t_dis9_u3",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(8),users.get(3), Status.PLANNED));
        for (Task t: tasks){
            taskService.save(t);
        }
    }
}
