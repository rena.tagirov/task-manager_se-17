package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tagirov.tm.api.repository.ProjectRepository;
import ru.tagirov.tm.api.repository.TaskRepository;
import ru.tagirov.tm.api.repository.UserRepository;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.dto.taskDTO.TaskDto;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;

import java.util.List;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void save(@NotNull Task task){
        taskRepository.save(task);
    }

    @Override
    public @NotNull List<Task> findAll(){
        return taskRepository.findAll();
    }

    @Override
    public @Nullable Task getOne(@NotNull final String taskId){
        return taskRepository.getOne(taskId);
    }

    @Override
    public void delete(@NotNull final Task task){
        taskRepository.delete(task);
    }

    @Override
    public void deleteAll(){
        taskRepository.deleteAll();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @NotNull List<Task> findAllByUser_Id(@NotNull String userId){
        return taskRepository.findAllByUser_Id(userId);
    }

    @Override
    public List<Task> findAllByProject_IdAndUser_Id(@NotNull String projectId, @NotNull String userId){
        return taskRepository.findAllByProject_IdAndUser_Id(projectId,userId);
    }

    //    Convert to DTO-----------------------------------------------------------------------------

    public TaskDto convertToDto(Task task){
        TaskDto taskDto = new TaskDto(task.getId(),
                task.getName(),
                task.getDescription(),
                task.getDateBegin(),
                task.getDateEnd(),
                task.getProject().getId(),
                task.getUser().getId(),
                task.getStatus());
        return taskDto;
    }

    public Task convertToEntity(TaskDto taskDto){
        User user = userRepository.getOne(taskDto.getUserId());
        Project project = projectRepository.getOne(taskDto.getProjectId());
        Task task = new Task(taskDto.getId(),
                taskDto.getName(),
                taskDto.getDescription(),
                taskDto.getDateBegin(),
                taskDto.getDateEnd(),
                project,
                user,
                taskDto.getStatus());
        return task;
    }

}
