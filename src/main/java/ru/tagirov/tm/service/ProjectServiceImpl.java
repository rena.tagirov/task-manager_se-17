package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tagirov.tm.api.repository.ProjectRepository;
import ru.tagirov.tm.api.repository.UserRepository;
import ru.tagirov.tm.api.service.ProjectService;
import ru.tagirov.tm.dto.projectDTO.ProjectDto;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;

import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserRepository userRepository;

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void save(@NotNull Project project){
        projectRepository.save(project);
    }

    @Override
    public @Nullable Project getOne(@NotNull final String projectId){
        return projectRepository.getOne(projectId);
    }

    @Override
    public @NotNull List<Project> findAll(){
        return  projectRepository.findAll();
    }

    @Override
    public void remove(@NotNull final Project project){
        projectRepository.delete(project);
    }

    @Override
    public void removeAll(){
        projectRepository.deleteAll();
    }

    @Override
    public void deleteAllByUser_Id(@NotNull String userId){
        projectRepository.deleteAllByUser_Id(userId);
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @Nullable List<Project> findAllByUserId(@NotNull String userId){
        return projectRepository.findAllByUserId(userId);
    }

    //    Convert to DTO-----------------------------------------------------------------------------

    public ProjectDto convertToDto(Project project){
        ProjectDto projectDto = new ProjectDto(project.getId(),
                project.getName(),
                project.getDescription(),
                project.getDateBegin(),
                project.getDateEnd(),
                project.getUser().getId(),
                project.getStatus());
        return projectDto;
    }

    public Project convertToEntity(ProjectDto projectDto){
        User user = userRepository.getOne(projectDto.getUserId());
        Project project = new Project(projectDto.getId(),
                projectDto.getName(),
                projectDto.getDescription(),
                projectDto.getDateBegin(),
                projectDto.getDateEnd(),
                user,
                projectDto.getStatus());
        return project;
    }
}

