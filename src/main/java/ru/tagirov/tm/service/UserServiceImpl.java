package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tagirov.tm.api.repository.UserRepository;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.dto.userDTO.UserDto;
import ru.tagirov.tm.entity.User;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void save(@NotNull User user) {
        userRepository.save(user);
    }

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void delete(@NotNull final User user) {
        userRepository.delete(user);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public User findByLogin(@NotNull String login) {
        return userRepository.findByLogin(login);
    }

    public User getOne (@NotNull String id){
        return userRepository.getOne(id);
    }

//    Convert to DTO-----------------------------------------------------------------------------

    public UserDto convertToDto(User user){
        UserDto userDto = new UserDto(user.getId(),
                user.getLogin(),
                user.getPassword(),
                user.getRole());
        return userDto;
    }

    public User convertToEntity(UserDto userDto){
        User user = new User(userDto.getId(),
                userDto.getLogin(),
                userDto.getPassword(),
                userDto.getRole());
        return user;
    }
}
