package ru.tagirov.tm.dto.taskDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TaskIdDto {

    private String taskId;

    public TaskIdDto(String taskId) {
        this.taskId = taskId;
    }
}
