package ru.tagirov.tm.dto.taskDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TaskUserIdDto {

    private String userId;

    public TaskUserIdDto(String userId) {
        this.userId = userId;
    }
}
