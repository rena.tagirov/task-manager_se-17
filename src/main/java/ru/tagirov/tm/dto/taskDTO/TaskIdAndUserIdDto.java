package ru.tagirov.tm.dto.taskDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TaskIdAndUserIdDto {

    private String taskId;
    private String userId;

    public TaskIdAndUserIdDto(String taskId, String userId) {
        this.taskId = taskId;
        this.userId = userId;

    }
}
