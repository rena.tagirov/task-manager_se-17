package ru.tagirov.tm.dto.taskDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TaskIdAndProjectIdDto {

    private String taskId;
    private String projectId;

    public TaskIdAndProjectIdDto(String taskId, String projectId) {
        this.taskId = taskId;
        this.projectId = projectId;
    }
}
