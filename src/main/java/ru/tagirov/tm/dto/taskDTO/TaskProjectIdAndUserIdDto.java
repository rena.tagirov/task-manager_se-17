package ru.tagirov.tm.dto.taskDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TaskProjectIdAndUserIdDto {

    private String projectId;
    private String userId;

    public TaskProjectIdAndUserIdDto(String projectId, String userId) {
        this.projectId = projectId;
        this.userId = userId;
    }
}
