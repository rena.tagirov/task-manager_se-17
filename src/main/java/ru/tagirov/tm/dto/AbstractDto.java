package ru.tagirov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractDto implements Serializable{

    @NotNull
    private String id;

    public AbstractDto(@NotNull final String id){
            this.id = id;
    }

}
