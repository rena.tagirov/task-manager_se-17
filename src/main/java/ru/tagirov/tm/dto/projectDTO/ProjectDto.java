package ru.tagirov.tm.dto.projectDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.dto.PurposeEntityDto;
import ru.tagirov.tm.enumeration.Status;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class ProjectDto extends PurposeEntityDto {

    public ProjectDto(@NonNull final String id,
                   @NonNull final String name,
                   @NonNull final String description,
                   @NonNull final Date dateBegin,
                   @NonNull final Date dateEnd,
                   @NonNull final String userId,
                   @NotNull final Status status){
        super(id, name, description, dateBegin, dateEnd, userId, status);
    }

}
