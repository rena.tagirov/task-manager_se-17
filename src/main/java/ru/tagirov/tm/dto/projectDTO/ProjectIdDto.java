package ru.tagirov.tm.dto.projectDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ProjectIdDto {

    private String projectId;

    public ProjectIdDto(String projectId) {
        this.projectId = projectId;
    }
}
