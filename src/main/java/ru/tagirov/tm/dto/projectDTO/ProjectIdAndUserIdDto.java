package ru.tagirov.tm.dto.projectDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ProjectIdAndUserIdDto {

    private String projectId;
    private String userId;

    public ProjectIdAndUserIdDto(String projectId, String userId) {
        this.projectId = projectId;
        this.userId = userId;
    }
}
