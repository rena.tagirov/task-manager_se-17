package ru.tagirov.tm.dto.userDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.dto.AbstractDto;
import ru.tagirov.tm.enumeration.Role;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class UserDto extends AbstractDto implements Serializable {

    public UserDto(@NonNull final String id,
                @NonNull final String login,
                @NonNull final String password,
                @NonNull final Role role) {
        super(id);
        this.login = login;
        this.password = password;
        this.role = role;
    }

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private Role role;
}
