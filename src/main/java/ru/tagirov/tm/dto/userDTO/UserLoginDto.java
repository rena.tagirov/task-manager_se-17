package ru.tagirov.tm.dto.userDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UserLoginDto {

    private String login;

    public UserLoginDto(String login) {
        this.login = login;
    }

}
