package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.dto.taskDTO.TaskDto;
import ru.tagirov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.util.List;


public interface TaskService {

    //    CRUD ----------------------------------------------------------------------

    void save(@NotNull final Task task);

    @Nullable
    List<Task> findAll();

    @Nullable Task getOne(@NotNull final String taskId);

    @Nullable
    void delete(@NotNull final Task task);

    void deleteAll();

    //    ALL ------------------------------------------------------------------------

    @NotNull
    List<Task> findAllByUser_Id(@NotNull final String userId);

    List<Task> findAllByProject_IdAndUser_Id(@NotNull String projectId, @NotNull String userId);

    //    Convert to DTO-----------------------------------------------------------------------------

    TaskDto convertToDto(Task task);

    Task convertToEntity(TaskDto taskDto);
}
