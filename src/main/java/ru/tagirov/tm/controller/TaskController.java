package ru.tagirov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.dto.taskDTO.TaskDto;
import ru.tagirov.tm.dto.taskDTO.TaskIdDto;
import ru.tagirov.tm.dto.taskDTO.TaskProjectIdAndUserIdDto;
import ru.tagirov.tm.dto.taskDTO.TaskUserIdDto;
import ru.tagirov.tm.entity.Task;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/findAll", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public List<TaskDto> findAll() throws SQLException {
        List<TaskDto> listTaskDto = new ArrayList<>();
        List<Task> listTask = taskService.findAll();
        for (Task task : listTask){
            listTaskDto.add(taskService.convertToDto(task));
        }
        return listTaskDto;
    }

    @RequestMapping(value = "/save", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String save(@RequestBody TaskDto taskDto) throws SQLException {
        taskService.save(taskService.convertToEntity(taskDto));
        return "ok";
    }

    @RequestMapping(value = "/update", //
            method = RequestMethod.PUT, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String update(@RequestBody TaskDto taskDto) throws SQLException {
        taskService.save(taskService.convertToEntity(taskDto));
        return "update ok";
    }

    @RequestMapping(value = "/delete", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String delete(@RequestBody TaskDto taskDto) throws SQLException {
        taskService.delete(taskService.convertToEntity(taskDto));
        return "delete ok";
    }

    @RequestMapping(value = "/deleteAll", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String deleteAll() throws SQLException {
        taskService.deleteAll();
        return "delete all ok";
    }

    @RequestMapping(value = "/getOne", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public TaskDto getOne(@RequestBody TaskIdDto taskIdDto) throws SQLException {
        TaskDto taskDto = taskService.convertToDto(taskService.getOne(taskIdDto.getTaskId()));
        return taskDto;
    }

    @RequestMapping(value = "/findAllByUser_Id", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public List<TaskDto> findAllByUser_Id(@RequestBody TaskUserIdDto taskUserIdDto) throws SQLException {
        List<TaskDto> listTaskDto = new ArrayList<>();
        List<Task> listTask = taskService.findAllByUser_Id(taskUserIdDto.getUserId());
        for (Task task : listTask){
            listTaskDto.add(taskService.convertToDto(task));
        }
        return listTaskDto;
    }

    @RequestMapping(value = "/findAllByProject_IdAndUser_Id", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public List<TaskDto> findAllByProject_IdAndUser_Id(@RequestBody TaskProjectIdAndUserIdDto taskProjectIdAndUserIdDto) throws SQLException {
        List<TaskDto> listTaskDto = new ArrayList<>();
        List<Task> listTask = taskService.findAllByProject_IdAndUser_Id(
                taskProjectIdAndUserIdDto.getProjectId(),
                taskProjectIdAndUserIdDto.getUserId());
        for (Task task : listTask){
            listTaskDto.add(taskService.convertToDto(task));
        }
        return listTaskDto;
    }

}
