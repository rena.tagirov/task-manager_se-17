package ru.tagirov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.tagirov.tm.api.service.ProjectService;
import ru.tagirov.tm.dto.projectDTO.ProjectDto;
import ru.tagirov.tm.dto.projectDTO.ProjectIdDto;
import ru.tagirov.tm.dto.projectDTO.ProjectUserIdDto;
import ru.tagirov.tm.entity.Project;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/findAll", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public List<ProjectDto> findAll() throws SQLException {
        List<ProjectDto> listProjectDto = new ArrayList<>();
        List<Project> listProject = projectService.findAll();
        for (Project project : listProject){
            listProjectDto.add(projectService.convertToDto(project));
        }
        return listProjectDto;
    }

    @RequestMapping(value = "/save", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String save(@RequestBody ProjectDto projectDto) throws SQLException {
        projectService.save(projectService.convertToEntity(projectDto));
        return "ok";
    }

    @RequestMapping(value = "/update", //
            method = RequestMethod.PUT, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String update(@RequestBody ProjectDto projectDto) throws SQLException {
        projectService.save(projectService.convertToEntity(projectDto));
        return "update ok";
    }

    @RequestMapping(value = "/remove", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String remove(@RequestBody ProjectDto projectDto) throws SQLException {
        projectService.remove(projectService.convertToEntity(projectDto));
        return "delete ok";
    }

    @RequestMapping(value = "/removeAll", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String removeAll() throws SQLException {
        projectService.removeAll();
        return "delete all ok";
    }

    @RequestMapping(value = "/getOne", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public ProjectDto getOne(@RequestBody ProjectIdDto projectIdDto) throws SQLException {
        ProjectDto projectDto = projectService.convertToDto(projectService.getOne(projectIdDto.getProjectId()));
        return projectDto;
    }

    @RequestMapping(value = "/findAllByUserId", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public List<ProjectDto> findAllByUserId(@RequestBody ProjectUserIdDto projectUserIdDto) throws SQLException {
        List<ProjectDto> listProjectDto = new ArrayList<>();
        List<Project> listProject = projectService.findAllByUserId(projectUserIdDto.getUserId());
        for (Project project : listProject){
            listProjectDto.add(projectService.convertToDto(project));
        }
        return listProjectDto;
    }

    @RequestMapping(value = "/deleteAllByUser_Id", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String deleteAllByUser_Id(@RequestBody ProjectUserIdDto projectUserIdDto) throws SQLException {
        projectService.deleteAllByUser_Id(projectUserIdDto.getUserId());
        return "delete all ok";
    }
}
