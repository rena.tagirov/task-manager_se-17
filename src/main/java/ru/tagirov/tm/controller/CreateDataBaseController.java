package ru.tagirov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tagirov.tm.util.EntityCreateUtil;

import java.sql.SQLException;
import java.text.ParseException;

@RestController
public class CreateDataBaseController {

    @Autowired
    private EntityCreateUtil entityCreateUtil;

    @RequestMapping("/")
    public String welcome() {
        return "WELCOME TO TASK MANAGER!.";
    }

    @RequestMapping("/create")
    public String createUser() throws SQLException, ParseException {
        entityCreateUtil.create();
        return "create - ok!";
    }
}
