package ru.tagirov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.dto.userDTO.UserDto;
import ru.tagirov.tm.dto.userDTO.UserLoginDto;
import ru.tagirov.tm.entity.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/findAll", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public List<UserDto> findAll() throws SQLException {
        List<UserDto> listUserDto = new ArrayList<>();
        List<User> listUser = userService.findAll();
        for (User user : listUser){
            listUserDto.add(userService.convertToDto(user));
        }
        return listUserDto;
    }

    @RequestMapping(value = "/save", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String save(@RequestBody UserDto userDto) throws SQLException {
        userService.save(userService.convertToEntity(userDto));
        return "ok";
    }

    @RequestMapping(value = "/update", //
            method = RequestMethod.PUT, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String update(@RequestBody UserDto userDto) throws SQLException {
        userService.save(userService.convertToEntity(userDto));
        return "update ok";
    }

    @RequestMapping(value = "/delete", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String delete(@RequestBody UserDto userDto) throws SQLException {
        userService.delete(userService.convertToEntity(userDto));
        return "delete ok";
    }

    @RequestMapping(value = "/deleteAll", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public String deleteAll() throws SQLException {
        userService.deleteAll();
        return "delete all ok";
    }

    @RequestMapping(value = "/findByLogin", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public UserDto findByLogin(@RequestBody UserLoginDto userLoginDTO) throws SQLException {
        UserDto userDto = userService.convertToDto(userService.findByLogin(userLoginDTO.getLogin()));
        return userDto;
    }
}
